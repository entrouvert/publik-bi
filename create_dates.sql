-- Crée une table de dates entre 2010 et 2020
DROP TABLE IF EXISTS dates;
CREATE TABLE dates AS (SELECT
        the_date.the_date::date AS date,
        to_char(the_date.the_date, 'TMday') AS day,
        to_char(the_date.the_date, 'TMmonth') AS month
    FROM
        generate_series('2010-01-01'::date, '2020-01-01'::date, '1 day'::interval)
    AS the_date(the_date));
