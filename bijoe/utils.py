import glob
import json

from django.conf import settings

from .schemas import Warehouse


def get_warehouses():
    warehouses = []
    for pattern in settings.BIJOE_SCHEMAS:
        for path in glob.glob(pattern):
            warehouses.append(Warehouse.from_json(json.load(open(path))))
    return warehouses
