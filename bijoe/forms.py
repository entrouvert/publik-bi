import datetime

from django import forms
from django.utils.translation import ugettext as _

from django_select2.forms import Select2MultipleWidget


class DateRangeWidget(forms.MultiWidget):
    def __init__(self, attrs=None):
        attrs2 = {'type': 'date'}
        if attrs:
            attrs2.update(attrs)
        widgets = (
            forms.DateInput(attrs=attrs2.copy()),
            forms.DateInput(attrs=attrs2.copy()),
        )
        super(DateRangeWidget, self).__init__(widgets, attrs=attrs)

    def decompress(self, value):
        if not value:
            return None, None
        return value


class DateRangeField(forms.MultiValueField):
    widget = DateRangeWidget

    def __init__(self, *args, **kwargs):
        # Or define a different message for each field.
        fields = (
            forms.DateField(required=False),
            forms.DateField(required=False),
        )
        super(DateRangeField, self).__init__(fields=fields, require_all_fields=False, *args,
                                             **kwargs)

    def compress(self, values):
        return values


class CubeForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.cube = cube = kwargs.pop('cube')
        super(CubeForm, self).__init__(*args, **kwargs)

        # filters
        for dimension in cube.dimensions:
            if not dimension.filter:
                continue
            field_name = 'filter__%s' % dimension.name
            if dimension.type is datetime.date:
                self.fields[field_name] = DateRangeField(
                    label=dimension.label.capitalize(), required=False)
            else:
                self.fields[field_name] = forms.MultipleChoiceField(
                    label=dimension.label.capitalize(),
                    choices=dimension.members,
                    required=False,
                    widget=Select2MultipleWidget())

        # group by
        choices = [(dimension.name, dimension.label) for dimension in cube.dimensions
                   if dimension.type not in (datetime.datetime, datetime.date)]
        self.fields['drilldown'] = forms.MultipleChoiceField(
            label=_('Group by'), choices=choices, required=False, widget=Select2MultipleWidget())

        # measures
        choices = [(measure.name, measure.label) for measure in cube.measures]
        self.fields['measures'] = forms.MultipleChoiceField(
            label=_('Measures'), choices=choices, widget=Select2MultipleWidget())
