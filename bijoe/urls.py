from django.conf.urls import patterns, include, url
from django.contrib import admin

import views

urlpatterns = patterns(
    '',
    url(r'^$', views.HomepageView.as_view(), name='homepage'),
    url(r'^(?P<warehouse>[^/]*)/$', views.WarehouseView.as_view(), name='warehouse'),
    url(r'^(?P<warehouse>[^/]*)/(?P<cube>[^/]*)/$', views.CubeView.as_view(), name='cube'),
    url(r'^admin/', include(admin.site.urls)),
)
