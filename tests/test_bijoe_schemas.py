# -*- coding: utf-8 -*-
from bijoe.schemas import Warehouse


def test_simple_parsing():
    Warehouse.from_json({
        'name': 'coin',
        'label': 'coin',
        'pg_dsn': 'dbname=zozo',
        'search_path': ['cam', 'public'],
        'cubes': [
            {
                'name': 'all_formdata',
                'label': 'Tous les formulaires',
                'fact_table': 'formdata',
                'key': 'id',
                'joins': [
                    {
                        'name': 'formdef',
                        'master': '{fact_table}.formdef_id',
                        'table': 'formdef',
                        'detail': 'formdef.id',
                    }
                ],
                'dimensions': [
                    {
                        'label': 'formulaire',
                        'name': 'formdef',
                        'type': 'integer',
                        'join': ['formdef'],
                        'value': 'formdef.id',
                        'value_label': 'formdef.label',
                        'order_by': 'formdef.label'
                    },
                    {
                        'name': 'receipt_time',
                        'label': 'date de soumission',
                        'join': ['receipt_time'],
                        'type': 'date',
                        'value': 'receipt_time.date'
                    }
                ],
                'measures': [
                    {
                        'type': 'integer',
                        'label': 'Nombre de demandes',
                        'expression': 'count({fact_table}.id)',
                        'name': 'count'
                    },
                    {
                        'type': 'integer',
                        'label': u'Délai de traitement',
                        'expression': 'avg((to_char(endpoint_delay, \'9999.999\') || \' days\')::interval)',
                        'name': 'avg_endpoint_delay'
                    },
                    {
                        'type': 'percent',
                        'label': 'Pourcentage',
                        'expression': 'count({fact_table}.id) * 100. / (select count({fact_table}.id) from {table_expression} where {where_conditions})',
                        'name': 'percentage'
                    }
                ]
            }
        ],
    })
