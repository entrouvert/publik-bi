-- Clean
DROP VIEW IF EXISTS bi_form;
DROP VIEW IF EXISTS bi_status;
DROP VIEW IF EXISTS bi_all_forms;
DROP VIEW IF EXISTS bi_encombrants;
DROP VIEW IF EXISTS bi_assainissement;
DROP VIEW IF EXISTS bi_eclairage_public;
DROP VIEW IF EXISTS bi_mobilier_urbain;
DROP VIEW IF EXISTS bi_proprete;
DROP VIEW IF EXISTS bi_reseaux_eau_potable;
DROP VIEW IF EXISTS bi_voirie;
DROP VIEW IF EXISTS bi_espaces_verts;
DROP TABLE IF EXISTS bi_poles;
DROP TABLE IF EXISTS bi_dates;
DROP TABLE IF EXISTS bi_backoffice_submission;

-- Create view functions

CREATE OR REPLACE FUNCTION public.month(a timestamp)
 RETURNS CHAR AS $$ select to_char($1, 'TMmonth') $$ LANGUAGE SQL IMMUTABLE ;
CREATE OR REPLACE FUNCTION public.day(a timestamp)
 RETURNS CHAR AS $$ select to_char($1, 'TMday') $$ LANGUAGE SQL IMMUTABLE ;

CREATE TABLE bi_poles (pole_commune varchar, pole varchar);
\copy bi_poles from 'poles.csv' WITH CSV DELIMITER ',' QUOTE '"'

CREATE TABLE bi_backoffice_submission (value boolean primary key, name varchar);
INSERT INTO bi_backoffice_submission VALUES
        ('t', 'Backoffice'),
        ('f', 'Frontoffice');

CREATE TABLE bi_dates AS (SELECT
        the_date.the_date::date AS date,
        date_part('day'::text, the_date.the_date) AS day,
        date_part('month'::text, the_date.the_date) AS month,
        date_part('year'::text, the_date.the_date) AS year
    FROM
        generate_series(
            (SELECT min(wcs_all_forms.receipt_time) AS min FROM wcs_all_forms),
            (SELECT max(wcs_all_forms.receipt_time) + '1 mon'::interval FROM wcs_all_forms),
            '1 day'::interval)
    AS the_date(the_date));

--  Encombrants

CREATE VIEW bi_encombrants AS (SELECT
        id AS id,
        formdef_id,
        receipt_time::date AS receipt_time,
        status AS status,
        backoffice_submission,
        commune_display AS pole_commune_ref
	FROM wcs_view_65_demande_denlevement_dencombrant AS f
    WHERE status != 'draft'
);

--  Assainissement et réseaux d'eau usée
CREATE VIEW bi_assainissement AS (SELECT
        id AS id,
        formdef_id,
        receipt_time::date AS receipt_time,
        status AS status,
        commune_display AS pole_commune_ref,
        backoffice_submission,
        nature_demande_display as nature_demande
	FROM wcs_view_101_assainissement_et_reseaux_d_eaux_usees AS f
    WHERE status != 'draft'
);



--  Éclairage public
CREATE VIEW bi_eclairage_public AS (SELECT
        id AS id,
        formdef_id,
        receipt_time::date AS receipt_time,
        status AS status,
        commune_display AS pole_commune_ref,
        backoffice_submission,
        nature_demande_display as nature_demande
	FROM wcs_view_84_reseaux_d_eau_potable_fuite_d_eau_coupur AS f
    WHERE status != 'draft'
);


--  Mobilier urbain
CREATE VIEW bi_mobilier_urbain AS (SELECT
        id AS id,
        formdef_id,
        receipt_time::date AS receipt_time,
        status AS status,
        commune_display AS pole_commune_ref,
        backoffice_submission,
        nature_demande_display as nature_demande
        FROM wcs_view_85_reseaux_d_eau_potable_fuite_d_eau_coupur AS f
    WHERE status != 'draft'
);

--  Propreté
CREATE VIEW bi_proprete AS (SELECT
        id AS id,
        formdef_id,
        receipt_time::date AS receipt_time,
        status AS status,
        commune_display AS pole_commune_ref,
        backoffice_submission,
        nature_demande_display as nature_demande
        FROM wcs_view_86_proprete AS f
    WHERE status != 'draft'
);

--  Réseaux d'eau potable
CREATE VIEW bi_reseaux_eau_potable AS (SELECT
        id AS id,
        formdef_id,
        receipt_time::date AS receipt_time,
        status AS status,
        commune_display AS pole_commune_ref,
        backoffice_submission,
        nature_demande_display as nature_demande
        FROM wcs_view_81_ep_reseaux_d_eau_potable_fuite_d_eau_cou AS f
    WHERE status != 'draft'
);

-- Voirie (chaussée, trottoir et accotement)
CREATE VIEW bi_voirie AS (SELECT
        id AS id,
        formdef_id,
        receipt_time::date AS receipt_time,
        status AS status,
        commune_display AS pole_commune_ref,
        backoffice_submission,
        nature_demande_display as nature_demande
        FROM wcs_view_87_voirie_chaussee_trottoir_et_accotement_1 AS f
    WHERE status != 'draft'
);

-- Espaces verts

CREATE VIEW bi_espaces_verts AS (SELECT
        id AS id,
        formdef_id,
        receipt_time::date AS receipt_time,
        status AS status,
        commune_display AS pole_commune_ref,
        backoffice_submission,
        nature_demande_display as nature_demande
        FROM wcs_view_89_espaces_verts AS f
    WHERE status != 'draft'
);

-- Les 7 formulaires pôles
CREATE VIEW bi_all_forms AS (
    SELECT formdef_id || '-' || id AS id, backoffice_submission, 'voirie' as form, receipt_time, status, pole_commune_ref FROM bi_voirie
        UNION ALL                                             
    SELECT formdef_id || '-' || id AS id, backoffice_submission, 'proprete' as form, receipt_time, status, pole_commune_ref FROM bi_proprete
        UNION ALL                                             
    SELECT formdef_id || '-' || id AS id, backoffice_submission, 'mobilier_urbain' as form, receipt_time, status, pole_commune_ref FROM bi_mobilier_urbain
        UNION ALL                                             
    SELECT formdef_id || '-' || id AS id, backoffice_submission, 'eclairage_public' as form, receipt_time, status, pole_commune_ref FROM bi_eclairage_public
        UNION ALL                                             
    SELECT formdef_id || '-' || id AS id, backoffice_submission, 'espaces_verts' as form, receipt_time, status, pole_commune_ref FROM bi_espaces_verts);

CREATE VIEW bi_form AS (SELECT distinct(form) from bi_all_forms);

CREATE VIEW bi_status AS (SELECT distinct(status) from bi_all_forms);
