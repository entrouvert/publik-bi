#!/bin/bash

set -e

STDOUT=`tempfile`
STDERR=`tempfile`
TEMP_DUMP=`tempfile`



(
        set -x
	cd ~/publik-bi/3m/
	export PLATFORM=${PLATFORM:-prod}
	DB=auquotidien-bi
	TEMPDB=$DB-temp


	trap "rm -f $TEMP_DUMP $LOGFILE" INT TERM EXIT
	dropdb $TEMPDB || true
	createdb $TEMPDB
	pg_dump -O auquotidien >$TEMP_DUMP
	psql $TEMPDB -f $TEMP_DUMP
	psql $TEMPDB -f 3m_$PLATFORM.sql


	# minimize downtime
	/sbin/start-stop-daemon --stop --pidfile slicer-$PLATFORM.pid --oknodo --retry 5
	psql -c "ALTER DATABASE \"$DB\" RENAME TO \"$DB-old\"" auquotidien
	psql -c "ALTER DATABASE \"$TEMPDB\" RENAME TO \"$DB\"" auquotidien
	/sbin/start-stop-daemon --start --background --make-pidfile \
	   --pidfile slicer-$PLATFORM.pid --oknodo --startas ./run-slicer.sh \
	   --chdir .
	dropdb $DB-old
) >$STDOUT 2>$STDERR

if [[ "$?" != "0" ]]; then
        S=$?
        cat $STDOUT
        cat $STDERR
	echo Exited with status $S
fi
