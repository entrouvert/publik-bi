#!/bin/sh

set -x -e

. ~/cubes/venv/bin/activate

cd ~/publik-bi/3m/
SLICER=`which slicer`
PLATFORM=${PLATFORM:-prod}

exec $SLICER serve slicer-$PLATFORM.ini >>log_$PLATFORM.log 2>&1
