from StringIO import StringIO
import sys
import linecache


def print_tb():
    exc_type, exc_value, tb = sys.exc_info()
    if exc_value:
        exc_value = unicode(str(exc_value), errors='ignore')
    error_file = StringIO()

    limit = None
    if hasattr(sys, 'tracebacklimit'):
        limit = sys.tracebacklimit
    print >>error_file, "Exception:"
    print >>error_file, "  type = '%s', value = '%s'" % (exc_type, exc_value)
    print >>error_file

    # format the traceback
    print >>error_file, 'Stack trace (most recent call first):'
    n = 0
    while tb is not None and (limit is None or n < limit):
        frame = tb.tb_frame
        function = frame.f_code.co_name
        filename = frame.f_code.co_filename
        exclineno = frame.f_lineno
        locals = frame.f_locals.items()

        print >>error_file, '  File "%s", line %s, in %s' % (filename, exclineno, function)
        linecache.checkcache(filename)
        for lineno in range(exclineno - 2, exclineno + 3):
            line = linecache.getline(filename, lineno, frame.f_globals)
            if line:
                if lineno == exclineno:
                    print >>error_file, '>%5s %s' % (lineno, line.rstrip())
                else:
                    print >>error_file, ' %5s %s' % (lineno, line.rstrip())
        print >>error_file
        if locals:
            print >>error_file, "  locals: "
            for key, value in locals:
                print >>error_file, "     %s =" % key,
                try:
                    repr_value = repr(value)
                    if len(repr_value) > 10000:
                        repr_value = repr_value[:10000] + ' [...]'
                    print >>error_file, repr_value,
                except:
                    print >>error_file, "<ERROR WHILE PRINTING VALUE>",
                print >>error_file
        print >>error_file
        tb = tb.tb_next
        n = n + 1

    print >>sys.stderr, error_file.getvalue()
